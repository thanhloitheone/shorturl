@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">URL Shortener</div>

                <div class="panel-body">
                        @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                </div>
                <div style="margin-left: 50px">
                    <form action="/converturl" method='POST'>
                        {{ csrf_field() }}
                        <div class="form-group">
                            <input type="text" name="longurl" class="form-cotrol" style="width: 400px" placeholder="Enter Url Website!">
                            <button type="submit" style="background-color: blue; color: white"> Convert </submit>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
