@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <b>ShortUrl:</b>
                    <a href="/{{ $datacovert['shorturl'] }}">127.0.0.1:8000/{{ $datacovert['shorturl'] }}</a>
                    <br>
                    <b>LongUrl:</b> {{ $datacovert['longurl'] }}
                    <br>
                    <b>Total Vistis:</b> {{ count($datevisits) }}
                </div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <div class="alert alert-success">
                        
                        @foreach ( $datevisits as $datavisit )
                            {{ $datavisit }} |
                        @endforeach

                    </div>

                    

                </div>
            </div>
        </div>
    </div>
    
</div>
<div class="row">
<div class="col-md-7 col-md-offset-2">
@include('website.chart')
</div>
</div>
@endsection