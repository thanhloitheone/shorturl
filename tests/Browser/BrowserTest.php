<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;

class BrowserTest extends DuskTestCase
{
    protected $datatest = [
        'longurl' => 'https://viblo.asia/p/redis-la-gi-157G5okARAje',
        'shorturl' => '0fdcfdd0'
    ];

    public function test_home_website()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://127.0.0.1:8000/')
                    ->assertSee('Convert Url');
        });
    }

    public function test_visit_statistic()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://127.0.0.1:8000/')
                    ->clickLink('Statistic')
                    ->assertPathIs('/statistic');
        });
    }

    public function test_enter_link_true_cover_long_url()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://127.0.0.1:8000/')
                    ->type('longurl', $this->datatest['longurl'])
                    ->press('Convert')
                    ->assertSee($this->datatest['longurl']);
        });
    }

    public function test_enter_link_false_cover_long_url()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://127.0.0.1:8000/')
                    ->type('longurl', 'zxc')
                    ->press('Convert')
                    ->assertSee('The longurl format is invalid.');
        });
    }

    public function test_visit_shorturl_rediect_to_longurl()
    {
        $this->browse(function (Browser $browser) {
            $browser->visit('http://127.0.0.1:8000/' . $this->datatest['shorturl'])
                    ->assertPathIs("/p/redis-la-gi-157G5okARAje");
        });
    }
}