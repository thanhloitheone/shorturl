<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Support\RedisUrl;
use App\Support\RedisVisits;
use Illuminate\Support\Facades\Redis;

class AppConvertController extends Controller
{
    public function index()
    {
        return view('website.index');
    }

    public function store(Request $request)
    {
        if (!$this->checkValidate($request)) {
            return;
        }
        $converturl = new RedisUrl;

        $urltrue = $converturl->setLongUrl($request['longurl']);

        if ($urltrue) {
            $shorturl = $converturl->convert();
            return redirect($shorturl . '/status');
        }

        return redirect('/errorwebsite');
    }

    public function show()
    {
        $listdatas = new RedisUrl;
        $listdataurl = $listdatas->listDataConvert();

        return view('website.listdata', compact('listdataurl'));
    }

    public function error()
    {
        return view('website.error');
    }

    public function status($shorturl)
    {
        $visit = new RedisVisits;
        $visit->setShortUrl($shorturl);

        $datevisits = $visit->statisticVisit();
        $datacovert = $visit->getData();
        $jsondate = $visit->jsonDataDate();
        $visitlinkhour = $visit->statisvisit();

        return view(
            'website.status',
            compact('datevisits', 'datacovert', 'jsondate', 'visitlinkhour')
        );
    }

    public function visit($shorturl)
    {
        $visit = new RedisVisits;

        $visit->setShortUrl($shorturl);

        $visit->addVisit();

        return redirect($visit->getLongUrl());
    }

    public function feature()
    {
        return view('website.feature');
    }

    public function featurewebsite($option)
    {
        switch ($option) {
            case 'backup':
                return Redis::SAVE();
                break;
            case 'configdir':
                return Redis::CONFIG('get', 'dir');
                break;
            case 'retore':
                return Redis::BGSAVE();
                break;
            default:
                return;
        }
    }

    public function chartjs()
    {
        return view('website.charts', compact('data', 'label'));
    }

    private function checkValidate($request)
    {
        $validate = $request->validate($this->rules(), $this->messages());

        $isurl = $validate['longurl'] != $request['longurl'];

        if ($isurl) {
            return false;
        }

        return true;
    }

    private function rules()
    {
        return [
            'longurl' => 'required | url | min: 10',
        ];
    }

    private function messages()
    {
        return [
            'longurl.required' => 'A url is required',
        ];
    }
}
