<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Convert extends Eloquent
{
    public $timestamps = false;
}
