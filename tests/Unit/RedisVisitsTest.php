<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Support\RedisVisits;

class RedisVisitsTest extends TestCase
{
    private $shorturl;
    private $stubvisits;

    protected $datatest = [
        'longurl' => 'https://viblo.asia/p/redis-la-gi-157G5okARAje',
        'shorturl' => '0fdcfdd0'
    ];

    public function setUp()
    {
        parent::setUp();

        $this->shorturl = new RedisVisits;
        $this->stubvisits = $this->createMock(RedisVisits::class);
    }

    public function test_set_shorturl()
    {
        $this->shorturl->setShortUrl($this->datatest['shorturl']);

        $this->assertEquals($this->datatest['shorturl'], $this->shorturl->getShortUrl());
    }

    public function test_get_longurl()
    {
        $this->shorturl->setShortUrl($this->datatest['shorturl']);
        $this->assertEquals($this->datatest['longurl'], $this->shorturl->getLongUrl());
    }

    public function test_get_data()
    {
        $this->shorturl->setShortUrl($this->datatest['shorturl']);
        $this->assertEquals($this->datatest, $this->shorturl->getData());
    }
}
