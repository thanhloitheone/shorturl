@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">

                @foreach ( $listdataurl as $shorturl => $longurl )
                    <div class="panel-heading">
                        <b>Short Url:</b> <a href="{{ $shorturl }}"> 127.0.0.1:8000/{{ $shorturl }}</a>
                        <br>
                        <b>Short Url Status:</b> <a href="{{ $shorturl }}/status"> 127.0.0.1:8000/{{ $shorturl }}/status</a>
                        <br>
                        <b>Long Url:</b> {{ $longurl }}
                    </div>
                @endforeach

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>

            </div>
        </div>
    </div>
</div>
@endsection