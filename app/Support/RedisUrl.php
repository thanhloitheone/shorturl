<?php

namespace App\Support;

use Illuminate\Support\Facades\Redis;
use App\Convert;
use App\Visit;

class RedisUrl
{
    private $longurl;

    const PREG_URL_LOCAL = '/127.0.0.1/i';
    const DATABASE_NOT_EXIST = 0;

    public function __construct()
    {
    }

    public function setLongUrl($longurl)
    {
        if ($this->checkUrl($longurl)) {
            $this->longurl = $longurl;
            return true;
        }

        return false;
    }

    private function checkUrl($longurl)
    {
        $exitslongurl = $this->checkPregMatch(self::PREG_URL_LOCAL, $longurl);

        if ($exitslongurl[0] == null) {
            return filter_var($longurl, FILTER_VALIDATE_URL);
        }

        return false;
    }

    private function checkPregMatch($pattern, $subject)
    {
        preg_match_all($pattern, $subject, $matches);
        return $matches;
    }

    public function convert()
    {
        $short_url = $this->hashShortUrl();

        $checkurl = $this->checkExistsShoruUrl($short_url);

        if ($checkurl == self::DATABASE_NOT_EXIST) {
            $this->insertRedis();

            $this->insertMongoDB();
        }

        return $short_url;
    }

    public function checkExistsShoruUrl($short_url)
    {
        return Redis::HEXISTS('convert', $short_url);
    }

    private function insertRedis()
    {
        $array_empty = [];

        $json_array_empty = json_encode($array_empty);

        Redis::HSET('convert', $this->hashShortUrl(), $this->longurl);
        Redis::HSET('visit', $this->hashShortUrl(), $json_array_empty);
    }

    private function insertMongoDB()
    {
        $isData = $this->isDataInDatabase($this->hashShortUrl());

        if ($isData == null) {
            $this->addDatabaseConvert();
            $this->addDatabaseVisit();

            return true;
        } else {
            return false;
        }
    }

    private function addDatabaseConvert()
    {
        $databaseconvert = new Convert;
        $databaseconvert->_id = $this->hashShortUrl();
        $databaseconvert->longurl = $this->longurl;
        $databaseconvert->save();
    }

    private function addDatabaseVisit()
    {
        $array_empty = [];
        $json_array_empty = json_encode($array_empty);

        $databaseconvert = new Visit;
        $databaseconvert->_id = $this->hashShortUrl();
        $databaseconvert->date = $json_array_empty;
        $databaseconvert->save();
    }

    private function isDataInDatabase($short_url)
    {
        return Convert::find($short_url);
    }

    public function hashShortUrl()
    {
        return hash('joaat', $this->longurl);
    }

    public function listKeyShortUrl()
    {
        return collect(Redis::HKEYS('convert'));
    }

    public function listDataConvert()
    {
        return collect(Redis::HGETALL('convert'));
    }
}
