<?php

namespace Tests\Unit;

use Tests\TestCase;
use App\Support\RedisUrl;

class RedisUrlTest extends TestCase
{
    private $longurl;
    private $stublink;

    protected $datatest = [
        'longurl' => 'https://viblo.asia/p/redis-la-gi-157G5okARAje',
        'shorturl' => '0fdcfdd0'
    ];

    public function setUp()
    {
        parent::setUp();

        $this->longurl = new RedisUrl;
        $this->stublink = $this->createMock(RedisUrl::class);
    }

    public function test_set_longurl_true()
    {
        $this->assertTrue($this->longurl->setLongUrl($this->datatest['longurl']));
    }

    public function test_convert_url()
    {
        $this->longurl->setLongurl($this->datatest['longurl']);

        $this->assertEquals($this->datatest['shorturl'], $this->longurl->convert());
    }

    public function test_create_short_url()
    {
        $this->longurl->setLongurl($this->datatest['longurl']);

        $this->assertEquals($this->datatest['shorturl'], $this->longurl->hashShortUrl());
    }
}
