<?php

namespace Tests\Feature;

use Tests\TestCase;

class AppConvertTest extends TestCase
{
    protected $datatest = [
        'longurl' => 'https://viblo.asia/p/redis-la-gi-157G5okARAje',
        'shorturl' => '0fdcfdd0'
    ];

    public function test_url_index()
    {
        $this->get('/')
            ->assertStatus(200);
    }

    public function test_url_statistic()
    {
        $this->get('/statistic')
            ->assertStatus(200);
    }

    public function test_covert_link_exist()
    {
        $this->post('/converturl', ['longurl' => $this->datatest['longurl']]);
        
        $urltest = '/' . $this->datatest['shorturl'] . '/status';

        $this->get($urltest)
            ->assertStatus(200);
    }

    public function test_covert_link_has_database()
    {
        $this->post('/converturl', ['longurl' => $this->datatest['longurl']]);

        $urltest = '/' . $this->datatest['shorturl'] . '/status';

        $this->get($urltest)
            ->assertSee($this->datatest['longurl'])
            ->assertSee($this->datatest['shorturl']);
    }
}