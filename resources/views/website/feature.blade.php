@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <a href="/featurewebsite/backup"> BACK UP </a>
                </div>
                <div class="panel-heading">
                        <a href="/featurewebsite/configdir"> Config Dir </a>
                </div>
                <div class="panel-heading">
                        <a href="/featurewebsite/retore"> RETORE </a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection