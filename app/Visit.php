<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model as Eloquent;

class Visit extends Eloquent 
{
    public $timestamps = false; 
}
