<?php

use App\Convert;
use App\Visit;
use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'AppConvertController@index');
Route::get('/featurewebsite', 'AppConvertController@feature');
Route::get('/featurewebsite/{option}', 'AppConvertController@featurewebsite');

Route::post('/converturl', 'AppConvertController@store');
Route::get('/errorwebsite', 'AppConvertController@error')->name('error');
Route::get('/statistic', 'AppConvertController@show');
Route::get('/chartsvistis', 'AppConvertController@chartjs');
Route::get('/{shorturl}', 'AppConvertController@visit');
Route::get('/{shorturl}/status', 'AppConvertController@status');