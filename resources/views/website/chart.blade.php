<canvas id="myChart" height="100" width="200"></canvas>
<script>
        var datalabels = [];
        var datadataset = [];

    @foreach( $visitlinkhour as $hour => $count )
        datalabels.push("{{ $hour }}");
        datadataset.push({{ $count }});
    @endforeach
    
var ctx = document.getElementById("myChart").getContext('2d');
var myChart = new Chart(ctx, {
    type: 'line',
    data: {
        labels: datalabels,
        datasets: [{
            label: 'Count Visits',
            data: datadataset,
            backgroundColor: [
                'rgba(255, 99, 132, 0.2)',
            ],
            borderColor: [
                'rgba(255,99,132,1)',
            ],
            borderWidth: 4
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero:false
                }
            }]
        }
    }
});
</script>