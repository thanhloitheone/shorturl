<?php

namespace App\Support;

use Illuminate\Support\Facades\Redis;
use Carbon\Carbon;

class RedisVisits
{
    private $shorturl;

    const PREG_URL_LOCAL = '/127.0.0.1/i';

    public function __construct()
    {
    }

    public function setShortUrl($shorturl)
    {
        $this->shorturl = $shorturl;
    }

    public function getShortUrl()
    {
        return $this->shorturl;
    }

    /*
     * Add Date To Table Visits Follow To Key Short Url
     *
     * @param string $shorturl
     * @param data $date
     *
     */
    public function addVisit()
    {
        $json_date_visit = Redis::HGET('visit', $this->shorturl);

        $add_date_visit = $this->addDateVisit($json_date_visit);

        Redis::HSET('visit', $this->shorturl, $add_date_visit);
    }

    public function addDateVisit($json_date_visit)
    {
        $array_date = json_decode($json_date_visit);

        array_push($array_date, $this->getDateNow());

        $add_date_visit = json_encode($array_date);

        return $add_date_visit;
    }

    private function getDateNow()
    {
        $nowdate = Carbon::now();

        return $nowdate->toDateTimeString();
    }

    public function statisticVisit()
    {
        $visit = Redis::HGET('visit', $this->shorturl);
        $array_date = json_decode($visit);
        return collect($array_date);
    }

    public function getLongUrl()
    {
        $longurl = Redis::HGET('convert', $this->shorturl);
        return ($longurl);
    }

    public function getData()
    {
        $longurl = Redis::HGET('convert', $this->shorturl);
        $data['shorturl'] = $this->shorturl;
        $data['longurl'] = $longurl;
        return ($data);
    }

    public function jsonDataDate()
    {
        return Redis::HGET('visit', $this->shorturl);
    }

    public function statisvisit()
    {
        $nowdate = Carbon::now()->toDateString(); // 12
        $array_date = json_decode($this->jsonDataDate());
        $visit_link_hour = [];
        
        for ($i = 0; $i <= 23 ; $i++) {
            $visit_link_hour[$i] = 0;
        }

        for ($i = count($array_date) - 1; $i >= 0; $i--) {
            if (strpos($array_date[$i], $nowdate) < 0) {
                break;
            }
            $hour = substr($array_date[$i], 11, 2);

            $visit_link_hour[(int)$hour]++;
        }
        
        return $visit_link_hour;
    }
}
